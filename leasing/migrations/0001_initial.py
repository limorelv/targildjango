# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-07 18:51
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('price', models.FloatField()),
                ('timestamp', models.DateTimeField(default=datetime.datetime(2016, 5, 7, 21, 51, 37, 584000))),
            ],
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('city', models.CharField(choices=[('\u05d7\u05d9\u05e4\u05d4', '\u05d7\u05d9\u05e4\u05d4'), ('\u05d9\u05e8\u05d5\u05e9\u05dc\u05d9\u05d9\u05dd', '\u05d9\u05e8\u05d5\u05e9\u05dc\u05d9\u05d9\u05dd'), ('\u05d9\u05d4\u05d5\u05d3', '\u05d9\u05d4\u05d5\u05d3')], max_length=200)),
                ('phone_num', models.CharField(max_length=20)),
                ('is_active', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='PriceList',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price_at_company', models.FloatField()),
                ('car', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='leasing.Car')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='leasing.Company')),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='company',
            field=models.ManyToManyField(to='leasing.Company'),
        ),
    ]
