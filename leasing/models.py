# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.


@python_2_unicode_compatible
class Car(models.Model):
    name = models.CharField(max_length=200)
    price = models.FloatField()
    timestamp = models.DateTimeField(default=datetime.now(), null=False)
    company = models.ManyToManyField('Company')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Company(models.Model):
    city_choices = ((u'חיפה', u'חיפה'),
                    (u'ירושליים', u'ירושליים'),
                    (u'יהוד',u'יהוד'))
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200, choices=city_choices)
    phone_num = models.CharField(max_length=20)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class PriceList (models.Model):
    company = models.ForeignKey('Company')
    car = models.ForeignKey('Car')
    price_at_company = models.FloatField()

    def __str__(self):
        return str(self.price_at_company)




